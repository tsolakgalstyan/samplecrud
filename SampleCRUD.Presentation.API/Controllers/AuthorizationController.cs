﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleCRUD.DAL.Handlers;
using SampleCRUD.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Presentation.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AuthorizationController : ControllerBase
	{	
		[HttpPost("Login")]
		public async Task<ActionResult> EmployeeLogin([FromBody] User user, [FromServices] Authorize authorizeUser)
		{
			var userMock = new User
			{
				Login = "admin",
				Password = "1234"
			};

			if (!string.Equals(userMock.Login, user.Login))
			{
				return Ok(new
				{
					Success = false,
					Message = "User not found."
				});
			}

			if (!string.Equals(userMock.Password, user.Password)) return Unauthorized();

			var response = await authorizeUser.GetJwtStringAsync(user.Login, "user");
			return Ok(response);

		}
	}
}
