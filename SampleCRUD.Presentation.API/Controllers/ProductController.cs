﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SampleCRUD.DAL.EF;
using SampleCRUD.DAL.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.Presentation.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProductController : ControllerBase
	{
		private SampleCRUDContext _context;

		public ProductController(SampleCRUDContext context)
		{
			_context = context;
		}

		// GET: api/<ProductController>
		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var products = await _context.Products.Select(p => new Product
															{
																Id = p.Id,
																Name = p.Name,
																IsAvailable = p.IsAvailable,
																Price = p.Price
															}).ToListAsync();
			return Ok(products);
		}

		// GET: api/<ProductsController>/5
		[HttpGet("{id}")]
		public async Task<ActionResult<ProductAll>> Get(int id)
		{
			var product = await _context.Products.FindAsync(id);

			if (product == null)
			{
				return Ok(new
				{
					Success = false,
					Message = "Product not found."
				});
			}

			return product;
		}

		// POST api/<ProductsController>
		[HttpPost]
		[Authorize]
		public async Task<ActionResult<ProductAll>> Post([FromBody] ProductAll product)
		{
			if (await _context.Products.AnyAsync(p => string.Equals(p.Name, product.Name)))
			{
				return Ok(new
				{
					Success = false,
					Message = $"Product with name {product.Name} already exist."
				});
			}

			_context.Products.Add(product);
			await _context.SaveChangesAsync();

			return CreatedAtAction("Get", new { id = product.Id }, product);
		}

		// PUT api/<ProductsController>/5
		[HttpPut("{id}")]
		[Authorize]
		public async Task<ActionResult<ProductAll>> Put(int id, [FromBody] ProductAll product)
		{
			if (id != product.Id)
			{
				return BadRequest();
			}

			_context.Entry(product).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!await _context.Products.AnyAsync(p => p.Id == id))
				{
					return Ok(new
					{
						Success = false,
						Message = "Product not found."
					});
				}
			}

			return NoContent();
		}

		// DELETE api/<ProductsController>/5
		[HttpDelete("{id}")]
		public async Task<ActionResult<ProductAll>> Delete(int id)
		{
			var product = await _context.Products.FindAsync(id);
			if (product == null)
			{
				return NotFound(new
				{
					Success = false,
					Message = "Product not found."
				});
			}

			_context.Products.Remove(product);
			await _context.SaveChangesAsync();

			return product;
		}
	}
}
