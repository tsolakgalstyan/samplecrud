﻿using Microsoft.EntityFrameworkCore;
using SampleCRUD.DAL.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleCRUD.DAL.EF
{
	public class SampleCRUDContext : DbContext
	{
		public DbSet<ProductAll> Products { get; set; }

		public SampleCRUDContext(DbContextOptions options) : base(options)
		{

		}

		public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
		{
			try
			{
				var insertedEntries = ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList();

				insertedEntries.ForEach(e =>
				{
					e.Property("Created").CurrentValue = DateTime.Now;
				});

				var updatedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToList();

				updatedEntities.ForEach(e =>
				{
					e.Property("Modified").CurrentValue = DateTime.Now;
				});
			}
			catch (Exception)
			{
				//throw;
			}

			return base.SaveChangesAsync(cancellationToken);
		}
	}
}
