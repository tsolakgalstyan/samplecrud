﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SampleCRUD.DAL.Models
{
	public class ProductAll : Product
	{
		public string Description { get; set; }

		[Required]
		public DateTime Created { get; set; }

		public DateTime? Modified { get; set; }
	}
}
