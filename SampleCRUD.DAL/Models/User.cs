﻿using SampleCRUD.DAL.Handlers;
using System.ComponentModel.DataAnnotations;

namespace SampleCRUD.DAL.Models
{
	public class User
	{
        private string _password;

        [Required]
        public string Login { get; set; }

        [Required]
        public string Password
        {
            get => _password;
            set => _password = PasswordHashing.CreateHash(value);
        }
    }
}
