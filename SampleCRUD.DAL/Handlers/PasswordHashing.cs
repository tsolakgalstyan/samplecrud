﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SampleCRUD.DAL.Handlers
{
	public static class PasswordHashing
    {
        private const string _secretKey = "You talk to God, you're religious; God talks to you, you're psychotic.";

        public static string CreateHash(string password)
        {
            string newStr;
            if (password.Trim().Length > 5)
            {
                var i3 = Convert.ToInt32(password.Trim().Length / (double)3);
                newStr = password.Trim().Substring(i3, i3) + password.Trim().Substring(0, i3) + _secretKey.Trim() + password.Trim().Substring(i3 * 2);
            }
            else
            {
                newStr = password.Trim() + _secretKey.Trim();
            }
            var newStrBytes = Encoding.UTF8.GetBytes(newStr);
            using (var sha3 = new SHA384Managed())
            {
                var resultByte = sha3.ComputeHash(newStrBytes);
                var result = Convert.ToBase64String(resultByte);
                return result;
            }
        }
    }
}
