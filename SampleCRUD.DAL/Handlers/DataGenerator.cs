﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SampleCRUD.DAL.EF;
using SampleCRUD.DAL.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SampleCRUD.DAL.Handlers
{
	public class DataGenerator
	{
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
			using var context = new SampleCRUDContext(
				serviceProvider.GetRequiredService<DbContextOptions<SampleCRUDContext>>());
			
			if (context.Products.Any())
			{
				return;
			}

			context.Products.AddRange(
				new ProductAll
				{
					Name = "Apple",
					Price = 499.99M,
					IsAvailable = true,
					Created = DateTime.Now
				},

				new ProductAll
				{
					Name = "Xiaomi",
					Price = 350.99M,
					IsAvailable = true,
					Created = DateTime.Now
				},

				new ProductAll
				{
					Name = "Samsung",
					Price = 400.49M,
					IsAvailable = false,
					Created = DateTime.Now,
					Description = "This item is sold out."
				});

			await context.SaveChangesAsync();
		}
    }
}
